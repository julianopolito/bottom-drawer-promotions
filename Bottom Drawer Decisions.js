var open = false;

/**
 * Used to load and execute javascript file. an be used cross-domain seamlessly.
 * @param file JS file name
 * @param callback Subscribe to get notified when script file is loaded
 **/
function require(file, callback) {
  // create script element

  var script = document.createElement("script");
  script.src = file;

  // monitor script loading
  // IE < 7, does not support onload
  if (callback) {
    script.onreadystatechange = function () {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        // no need to be notified again
        script.onreadystatechange = null;
        // notify user
        callback();
      }
    };

    // other browsers
    script.onload = function () {
      callback();
    };
  }

  // append and execute script
  document.documentElement.firstChild.appendChild(script);
}

(function() {



    /**
     * @function buildBindId
     * @param {Object} context
     * @description Create unique bind ID based on the campaign and experience IDs.
     */
    function buildBindId(context) {
        return `${context.campaign}:${context.experience}`;
    }

    function apply(context, template) {
        const contentZoneSelector = 'body';
        console.log("apply running");//, template(context));
        console.log("carousel prods: ", context.promos);
        //document.documentElement.style.setProperty('--content-bg-color', context.bgolor);

        /**
         * The pageElementLoaded method waits for the content zone to load into the DOM
         * before rendering the template. The observer element that monitors for the content
         * zone element to get inserted into its DOM node is set to "body" by default.
         * For performance optimization, this default can be overridden by adding
         * a second selector argument, which will be used as the observer element instead.
         *
         * Visit the Template Display Utilities documentation to learn more:
         * https://developer.evergage.com/campaign-development/web-templates/web-display-utilities
         */
        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded(contentZoneSelector)
            .then((element) => {
                const html = template(context);
                //Evergage.cashDom(element).html(html);
                if(document.querySelector('#menuSlideContent_mcptpl') == null){
                  document.body.insertAdjacentHTML('beforeend',html);
                
                  if(!window.jQuery){
                    require("//code.jquery.com/jquery-3.6.0.min.js", function () {
                      console.log("jquery loaded", $('#menuSlideContent_mcptpl'));
                      open = true;
                      $('#menuSlideButton_mcptpl').click(function() {
                          if(open === false) {
                              $('#menuSlideContent_mcptpl').animate({ height: '280px' });
                              $(this).css('backgroundPosition', 'bottom left');
                              open = true;
                          } else {
                              $('#menuSlideContent_mcptpl').animate({ height: '0px' });
                              $(this).css('backgroundPosition', 'top left');
                              open = false;
                          }
                      }); 
                    $('#menuSlideButton_mcptpl').trigger( "click" );  
                    });
                  }else{
                      console.log("jquery exists",$('#menuSlideContent_mcptpl'));
                      open = true;
                      $('#menuSlideButton_mcptpl').click(function() {
                          if(open === false) {
                              $('#menuSlideContent_mcptpl').animate({ height: '280px' });
                              $(this).css('backgroundPosition', 'bottom left');
                              open = true;
                          } else {
                              $('#menuSlideContent_mcptpl').animate({ height: '0px' });
                              $(this).css('backgroundPosition', 'top left');
                              open = false;
                          }
                      }); 
                      $('#menuSlideButton_mcptpl').trigger( "click" ); 
                  }
                  document.documentElement.style.setProperty('--content-bg-color', context.bgcolor);
                  document.documentElement.style.setProperty("--item-box-bg","url("+context.itembgURL+")");
                  //console.log("returning template ", context.bgcolor);
                }
                

            }); 
    }

    function reset(context, template) {
        Evergage.DisplayUtils.unbind(buildBindId(context));
        Evergage.cashDom("#menuSlideContainer_mcptpl").remove();
    }

    function control(context) {
        
    }

    registerTemplate({
        apply: apply,
        reset: reset,
        control: control
    });

})();
