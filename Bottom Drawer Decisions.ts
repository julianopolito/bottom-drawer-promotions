import { ContextualBanditConfig, decide } from "corvus";
import { ItemReference } from "common";

function isCdnOrExternalImage(asset?: Asset) {
    return asset?.type === "CdnImage" || asset?.type === "ExternalImage";
}

export class PromotionSearchOptions implements Search<string> {

    search(context: GearLifecycleContext, searchString: string): ItemReference[] {
        if (!searchString) return [];

        const promos: Promotion[] = context.services.catalog.findByName("Promotion", searchString) as Promotion[];
        return promos.reduce((allPromos: ItemReference[], promo: Promotion) => {
            const promoItem = {
                itemType: "Promotion",
                id: promo.id,
                label: promo.attributes["name"] != null ? promo.attributes["name"].value : promo.id,
            } as ItemReference;
            allPromos.push(promoItem);
            return allPromos;
        }, []);
    }
}

export class AssetLookupOptions implements Lookup<string> {

    fallbackArm: ItemReference

    constructor(fallbackArm: ItemReference) {
        this.fallbackArm = fallbackArm;
    }

    lookup(context: GearLifecycleContext): string[] {
        if (!this.fallbackArm) return [];

        const fullPromo: Promotion = context.services.catalog.findItem("Promotion", this.fallbackArm.id) as Promotion;
        if (!fullPromo || !fullPromo.assets) return [];

        return fullPromo.assets.reduce((contentZones: string[], asset: Asset) => {
            if (isCdnOrExternalImage(asset) && asset?.contentZones) {
                asset.contentZones.forEach(zone => { if (!(zone in contentZones)) contentZones.push(zone) });
            }
            return contentZones;
        }, []);
    }
}

export class EinsteinDecisionsTemplate implements CampaignTemplateComponent {

    @header(' ')
    @headerSubtitle('What content zone to apply the template to.')

    @hidden(true)
    forHeaderSubtitle;

    @title("CTA Text")
    ctaText:String = "What's New"; 
    bgcolor:String = "#222222";
    //itembgURL:String = "https://wallpaperaccess.com/full/1201180.jpg";
     /* Developer Controls
     */

    @hidden(true)
    maximumNumberOfProducts = 4;
    contentZoneFilter = "Bottom Drawer";
    @header(" ")
    @headerSubtitle('Filter promotions to consider from the Promotions Catalog by content zone - doesnt have to be the same')

    @title("Show Text")
    @subtitle("Define if you want to show text, or only the bgimage")
    showtext:Boolean = true;

    run(context: CampaignComponentContext) {
        const banditConfig: ContextualBanditConfig = {
            maxResults: this.maximumNumberOfProducts,
            contentZone: this.contentZoneFilter
        } as ContextualBanditConfig;

        let promotions: Promotion[] = decide(context, banditConfig, null) as Promotion[];

        const fetchImageUrl = (promotions: Promotion[], contentZone: string): Object[] => {
            let promoAssets:Object[] = [];

            promotions.forEach(function(promotion:Promotion, index:number){
                let assetItem:Object = {imageUrl:"", url:"", title:"", desc:"", id:"", bgimage:""};

                if (!promotion || !promotion.assets) {
                    promoAssets.push(assetItem);
                }else{
                    for (const asset of promotion.assets) {
                        if (!isCdnOrExternalImage(asset)) continue;
                        if (asset.contentZones?.includes("fbpixel")) {
                            assetItem["imageUrl"] = (asset as ImageAsset).imageUrl;
                            promoAssets.push(assetItem);
                            break;
                        }
                    }
                    /*if (this.fallbackAsset && this.fallbackArm?.id === promotion.id) {
                        for (const asset of promotion.assets) {
                            if (!isCdnOrExternalImage(asset)) continue;
                            if (asset.contentZones?.includes(this.fallbackAsset)) {
                                assetItem["imageUrl"] = (asset as ImageAsset).imageUrl;
                                promoAssets.push(assetItem);
                                break;
                            }
                        }
                    }*/
                }
                assetItem["url"] = promotion.attributes?.url?.value;
                assetItem["title"] = promotion.attributes?.name?.value;
                assetItem["desc"] = promotion.attributes?.description?.value;
                assetItem["bgimage"] = promotion.attributes?.bgimage?.value;
                assetItem["id"] = promotion.id;
            });

            return promoAssets;
        };

        let promos: Object[] = fetchImageUrl(promotions, this.contentZoneFilter);

        return {
            promos: promos, 
            ctaText: this.ctaText, 
            bgcolor:this.bgcolor,
            showtext:this.showtext
            };
    }

}

